package com.spendoptima.domain;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

/**
 * Created by krzychu on 14.04.16.
 */
public abstract class UseCase {

    private final Scheduler executionScheduler;
    private final Scheduler postExecutionScheduler;

    private Subscription subscription = Subscriptions.empty();

    protected UseCase(Scheduler executionScheduler,
                      Scheduler postExecutionScheduler){
        this.executionScheduler = executionScheduler;
        this.postExecutionScheduler = postExecutionScheduler;
    }

    public abstract Observable buildUseCaseObservable();

    public void execute(Subscriber useCaseSubscriber) {
        this.subscription = this.buildUseCaseObservable()
                .subscribeOn(executionScheduler)
                .observeOn(postExecutionScheduler)
                .subscribe(useCaseSubscriber);
    }

    public void unsubscribe() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
