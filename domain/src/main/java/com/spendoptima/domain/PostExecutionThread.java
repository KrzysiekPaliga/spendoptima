package com.spendoptima.domain;

import rx.Scheduler;

/**
 * Created by krzychu on 14.04.16.
 */
public interface PostExecutionThread {
    Scheduler getScheduler();
}
