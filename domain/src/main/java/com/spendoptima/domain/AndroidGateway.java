package com.spendoptima.domain;

import rx.Observable;
import rx.Subscriber;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

/**
 * Created by krzychu on 14.04.16.
 */
public interface AndroidGateway {

    void execute(Subscriber subscriber);
}