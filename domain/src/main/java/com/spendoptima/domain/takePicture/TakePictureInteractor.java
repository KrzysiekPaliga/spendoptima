package com.spendoptima.domain.takePicture;

import com.spendoptima.domain.AndroidGateway;
import com.spendoptima.domain.PostExecutionThread;
import com.spendoptima.domain.ThreadExecutor;
import com.spendoptima.domain.UseCase;

import java.util.concurrent.Callable;

import rx.Observable;
import rx.Scheduler;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

/**
 * Created by krzychu on 14.04.16.
 */
public class TakePictureInteractor extends UseCase {

    private final AndroidGateway androidGateway;
    private BehaviorSubject<String> subject;//Observable observable;

    public TakePictureInteractor(AndroidGateway androidGateway,
                                 Scheduler executionScheduler,
                                 Scheduler postExecutionScheduler) {
        super(executionScheduler, postExecutionScheduler);
        this.androidGateway = androidGateway;
    }


    @Override
    public Observable buildUseCaseObservable() {
        //return androidGateway.takePicture(subject);
        return null;
    }

}
