package com.spendoptima.domain;

import java.util.concurrent.Executor;

/**
 * Created by krzychu on 14.04.16.
 */
public interface ThreadExecutor extends Executor {
}
