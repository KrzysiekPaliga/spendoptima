package com.spendoptima.presentation.android.mobile;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.spendoptima.domain.AndroidGateway;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

/**
 * Created by krzychu on 14.04.16.
 */
public class Android implements AndroidGateway {

    private final int REQUEST_CODE_TAKE_PICTURE = 1;

    private final Activity activity;
    private final PublishSubject<Uri> uriSubject;
    private final PublishSubject<Intent> intentSubject;

    public Android(Activity activity, PublishSubject<Uri> uriSubject, PublishSubject<Intent> intentSubject) {
        this.activity = activity;
        this.uriSubject = uriSubject;
        this.intentSubject = intentSubject;
    }

    @Override
    public void execute(Subscriber subscriber) {
        this.buildObservable().subscribe(subscriber);
    }
    private Observable buildObservable() {
        Observable<String> combinedObservable = intentSubject.zipWith(uriSubject, new Func2<Intent, Uri, String>() {
            @Override
            public String call(Intent intent, Uri uri) {
                return uri.toString();
            }
        });

        //TODO subscribe subject to observable
        dispatchTakePictureIntent().subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uriSubject);

        return combinedObservable;
    }

    private Observable<Uri> dispatchTakePictureIntent() {
        return Observable.create(new Observable.OnSubscribe<Uri>() {
            @Override
            public void call(Subscriber<? super Uri> subscriber) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        subscriber.onNext(Uri.fromFile(photoFile));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(photoFile));
                        activity.startActivityForResult(takePictureIntent, REQUEST_CODE_TAKE_PICTURE);
                    }
                }
            }
        });
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "SpendOptima_" + timeStamp + "_";
//        File storageDir = Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_PICTURES);
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        return image;
    }
}
