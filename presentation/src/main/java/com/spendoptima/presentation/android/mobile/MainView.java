package com.spendoptima.presentation.android.mobile;

import android.net.Uri;

/**
 * Created by krzychu on 21.04.16.
 */
public interface MainView {

    void displayPicture(Uri uri);
}
