package com.spendoptima.presentation.android.mobile;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.spendoptima.domain.UseCase;
import com.spendoptima.domain.takePicture.TakePictureInteractor;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

/**
 * Created by krzychu on 15.04.16.
 */
public class MainPresenterImpl {

    private Android android;
    private final MainView mainView;

    private PublishSubject<Intent> intentSubject;
    private PublishSubject<Uri> uriSubject;

    public MainPresenterImpl(Activity activity) {
        this.mainView = (MainView) activity;
    }

    public void takePicture(PublishSubject<Intent> intentSubject) {

        this.intentSubject = intentSubject;

        this.uriSubject = PublishSubject.<Uri>create();

        this.android = new Android((Activity) mainView, uriSubject, intentSubject);

        this.android.execute(new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object o) {
                String string = (String) o;
                Uri uri = Uri.parse(string);
                displayPicture(uri);
            }
        });

        UseCase takePictureUseCase = new TakePictureInteractor(android,
                AndroidSchedulers.mainThread(),
                AndroidSchedulers.mainThread());



    }

    public void displayPicture(Uri uri) {
        mainView.displayPicture(uri);
    }
}
